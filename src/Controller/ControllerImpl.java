package Controller;

import Model.Model;

public class ControllerImpl implements Controller {

	private Model model;
	
	public ControllerImpl(final Model model) {
		this.model = model;
	}
	
	@Override
	public void changeNum() {
		model.setNum();

	}

}

