package Model;

import java.util.*;

public class ModelImpl implements Model {

	private static final int LIMIT = 100;
	private Random rnd;
	private int num;
	
	public ModelImpl() {
		this.rnd = new Random();
	}
	
	public void setNum() {
		this.num = this.rnd.nextInt(LIMIT+1);//numeri da 1 a 100
	}

	@Override
	public int getNum() {
		return this.num;
	}

	
	
	
}
