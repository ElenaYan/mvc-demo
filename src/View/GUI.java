package View;

import javax.swing.JButton;
import javax.swing.JFrame;

import Controller.Controller;
import Model.Model;

public class GUI extends JFrame {
	
	private Controller controller;
	private Model model;
	
	public GUI(final Controller controller, final Model model) {
		this.controller = controller;
		this.model = model;
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		final JButton jb = new JButton(" ");
		
		this.getContentPane().add(jb);
		jb.addActionListener(e->{
			controller.changeNum();//alla pressione del pulsante cambia il numero
			jb.setText(Integer.toString(model.getNum()));//aggiorno il numero prendo i dati dal model 
														// approcio di tipo pull
		});
		this.pack();
		this.setVisible(true);
	}

	

}
